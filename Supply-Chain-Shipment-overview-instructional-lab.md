<img src="images/skilluplogo.png" width="200"/>

# Overview

The final project for this course has several steps that you must complete. The high-level step list given below will help to provide you with an overview of the whole project. The project is divided into smaller labs with detailed instructions for each step. You must complete all labs to complete the project successfully.

## Project Breakdown

- IBM cloud Account Creation.

- Watson Studio Creation .

- Creating a project and jupyter notebook in Watson studio.




- Use the following dataset .

  <a href="https://gitlab.com/pratikshapv/sko-capstone-project/-/blob/main/data/Supply_Chain_Shipment_Pricing_Data.csv">Supply_Chain_Shipment_Pricing_Data</a> 

- Explicitly mention the sections in the project. An example is given below.
    -   Import packages
    -   Load the data set
    -   Clean and engineer the dataset
    -   Perform EDA
    -   Create model(s)
    -   Test the model

## Submit your project for grading
What all are required to be  submitted and in what format. The responses can be:

1. Screenshots
2. URLs etc.

## Next Steps
To complete the lab, follow the step-by-step instructions given in the next module. *Note: Please include a module after this which throws more light into what needs to be done. 

## Author(s)
[Pratiksha Verma](https://www.linkedin.com/in/pratiksha-verma-6487561b1/)


## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2022-04-08 | 1.0 | Pratiksha Verma | Created Initial Version|

## <h3 align="center"> © SkillUp Technologies 2022. All rights reserved. <h3/>

<img src="images/skilluplogo.png" width="200"/>

# Introduction
## **Estimated time needed:** 120 mins

### Scenario
X is an Industrial company that manufactures Antiretroviral (ARV) and HIV lab products on large scale production.

In addition, the company provides Antiretroviral (ARV) and HIV lab shipments to supported countries.

Nowadays, as consumption and production are growing enormously fast, companies are seeking for costs reduction aimed at ensuring competitiveness. In manufacturing companies, supply chain expenses play a colossal role in the cost of the final product.

Supply chain is the overall processes of the product move-ment starting from the supplier to the end customer. Supply chain costs compose around 55% of the total product cost.  

Supply  chain  system consists  of  three processes:  sourcing, production  and  distribu-tion. Each  of  them is  special  in  terms of  supply  chain functioning. A supply  chain is a dynamic  process  and  includes information rows,  production,  warehousing services, packaging services  and  transportation components.

Since the supply chain has a big infuence on the cost of the final product,therefore the company wants to continually improve the processes within the supply chain  and  thereby reduce the overall cost. 

They also want to improve product distribution system.Shortcomings have been identified in the distribution system (late deliveries) that generate complaints and claims by the customer who purchases products through their company, Because these complaints ultimately translate into financial losses (customers refuse to use the service again and share negative experiences on social media).

Now they need your help in analyzing their Supply Chain Shipment Pricing Data and building and implementing a prediction model for estimating the risk of late delivery. 

You will be provided with the data containing shipment distribution information such as id, project code, pq #, po/so #, asn/dn #,country, managed by,fulfill via,vendor inco term,shipment mode,pq first sent to client date, po sent to vendor date,scheduled delivery date,delivered to client date, delivery recorded date,product group,sub classification, vendor, item description,molecule/test type, brand, dosage, dosage form, unit of measure (per pack), line item quantity,line item value, pack price,unit price,manufacturing site,first line designation, weight (kilograms),freight cost (usd),and line item insurance (usd).


As a prospective Data Scientist who has an inherent abilty to discover hidden pattern and useful insights , you are given a role  to analyze the data and build an effective predictive model.

Analyzing the data building a model to predict the late delivery in distribution system is extremely helpful for the company because they can then accordingly change and upgrade their process of shipment to reach on time to their customers and optimise their business model and revenue.


## Review Criteria – 30 marks total 
The capstone project is divided into n (replace with the number) modules. Each module has a quiz followed by a final submission that is graded (by your peers in this course or by the instructors). The grading is divided as follows:
- Module 1-IBM cloud account creation (1 point)
- Module 2 -Watson studio Creation(2 point)
- Module 2 -Notebook Creation using Python (1 point)
- Module 3-Dataset added as an data asset in Watson Studio(1 point)
- Final Submission-11 Problem statements related to Real Time Case Study with screenshots and URL (25 points)

Totalling to 30 points.

## Next Steps
Be sure to read the capstone overview before starting with the step-by-step instructions.

## Author(s)
[Pratiksha Verma](https://www.linkedin.com/in/pratiksha-verma-6487561b1/)


## Changelog
| Date |Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2022-04-11 | 1.0 |Pratiksha Verma| Created Initial Version |

## <h3 align="center"> © SkillUp Technologies 2022. All rights reserved. <h3/>
